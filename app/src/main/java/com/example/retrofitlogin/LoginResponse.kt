package com.example.retrofitlogin

data class LoginResponse(val status:String, val message:String, val data:User)