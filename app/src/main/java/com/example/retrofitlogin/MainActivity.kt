package com.example.retrofitlogin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressMainActivityprogressBar.visibility = View.GONE

        LoginMainActivityButton.setOnClickListener {


            var email = usernameMainActivityEditText.text.toString().trim()
            var password = passwordMainActivityPasswordText.text.toString().trim()
            var userType: Int = 1


            if (email.isEmpty()) {
                usernameMainActivityEditText.error = "Email required"
                usernameMainActivityEditText.requestFocus()
                return@setOnClickListener
  //              hideLoading()
            }
            if (password.isEmpty()){
                passwordMainActivityPasswordText.error ="Password required"
                passwordMainActivityPasswordText.requestFocus()
                return@setOnClickListener
//                hideLoading()
            }


            showLoading()

            if (isNetworkConnected()) {



                RetrofitObject.instance.userLogin(email, password, userType)
                        .enqueue(object : Callback<LoginResponse> {
                            override fun onResponse(
                                call: Call<LoginResponse>,
                                response: Response<LoginResponse>
                            ) {


                                if (response.body()?.status == "success") {
                                    Toast.makeText(
                                        applicationContext,
                                        "Login Success",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    startActivity(
                                        Intent(
                                            this@MainActivity,
                                            HomeActivity::class.java
                                        )
                                    )
                                    hideLoading()
                                } else if (response.body()?.status == "error") {
                                    Toast.makeText(applicationContext,
                                        "Login error",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    hideLoading()
                                } else {
                                    Toast.makeText(
                                        applicationContext,
                                        "Some error occurred try again",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    hideLoading()
                                }

                            }

                            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG)
                                    .show()
                                hideLoading()

                            }


                        })

            }
            else {
                hideLoading()
                Toast.makeText(this, "Connect to internet", Toast.LENGTH_LONG).show()
            }

        }

    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

}