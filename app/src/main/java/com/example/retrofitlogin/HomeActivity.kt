package com.example.retrofitlogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        logoutHomeActivityButton.setOnClickListener {
            startActivity(Intent(this ,MainActivity::class.java))
            finish()
        }

    }

    override fun onBackPressed() {
        startActivity(Intent(this ,MainActivity::class.java))
        finish()
    }
}